
public class Titular {
	
	private String nombre;
    private int saldo;
    private String nombre_banco;

    // constructor
    public Titular() {

	System.out.println("\r\n\t\t ** Se ha iniciado una nueva sesión. **");

    }

    // get
    // Get Nombre
    public String getNombre() {

	return this.nombre;
    }

    // Get Saldo
    public int getSaldo() {

	return this.saldo;

    }

    // Get Nombre Banco
    public String getNombre_Banco() {

	return this.nombre_banco;

    }

    // set
    // Set Nombre
    public void setNombre(String N) {

	this.nombre = N;

    }

    // Set Saldo
    public void setSaldo(int S) {

	this.saldo = S;

    }

    // Set nombre Banco
    public void setNombre_Banco(String NB) {

	this.nombre_banco = NB;
    }

}

