import java.util.ArrayList;

class Bibliotecario {
	
	private ArrayList <Biblioteca> biblioteca;
	
	public Bibliotecario(){
	biblioteca = new ArrayList<>();
	}
	
	public void agregar(String nombre_libro, String autor) {
		Biblioteca nuevo = new Biblioteca(nombre_libro, autor);
		biblioteca.add(nuevo);
		
	}
	
	public void visualizar () {
		for(int i = 0; i < biblioteca.size(); i++) {
			System.out.println("Nombre Libro: " + biblioteca.get(i).getNombreLibro() 
					+ "\nAutor: "+ biblioteca.get(i).getAutor());
		}
	}
}
