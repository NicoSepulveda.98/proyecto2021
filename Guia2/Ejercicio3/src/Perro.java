import java.util.Scanner;

public class Perro { 


	public static void main(String[] args) {
		
		VidaPerro1 vida_diaria = new VidaPerro1();
		Scanner scaner = new Scanner(System.in);
		
		vida_diaria.setPerro("Chiguageño");
		vida_diaria.setLadrar("Guau Guau");
		
		System.out.println("\r\n\t * ¡BIENVENIDO A LA VIDA DE TU PERRO!");
		System.out.println("\r\n ¿Deseas saber la vida de tu perro " + vida_diaria.getPerro() + "?\n");
		System.out.println("¡Tu perro se encuentra en su zona preferida, el patio!.");
		System.out.println("\nTu perro ladra... \r\t\n * " + vida_diaria.getLadrar());
		System.out.println("\nTu perro localiza algo. ¿Qué es?\n"
				+ "\r\n\t * PISTA: DICE *MIAU* .TIENE COLA Y 4 PATAS");
		System.out.println("\nIngrese lo que vio: ");
		String presa = scaner.nextLine();
		vida_diaria.setRabia(presa);
		
		System.out.println("\nTu perro quiere jugar \r\t\n * " + vida_diaria.getLadrar());
		System.out.println("\n¿Quieres jugar a lanzar la pelota?"
				+ "\n> Presiona 1 si quieres lanzar la pelota"
				+ "\n> Sino tu mascota se dormira.");
		String juego = scaner.nextLine();
		vida_diaria.setJugar(juego);
		
		System.out.println("\r\n\t * CANSANCIO");
		vida_diaria.descanso();
		

	}

}
