package Ejercicio2;
import java.util.ArrayList;

public class Controlador {
	private int matricula = 2018430017;

	Controlador(){
		this.matricula = matricula;

		
		ArrayList <String> modulos = new ArrayList<String>();

		modulos.add("Calculo II");
		modulos.add("Probabilidad y Estadistica");
		modulos.add("Programacion Avanzada");
		modulos.add("Fisica");
		modulos.add("Bioquimica");
		modulos.add("COE 3");
		
		
		System.out.println("Estudiante:\n");
		Comunes comunes = new Comunes ("Dayiana", " Quezada", matricula);
		comunes.getBiblio();

		System.out.println("Cursos:\n" + modulos);
		System.out.println("");
		
		System.out.println("Empleado:\n");
		Empleados empleados = new Empleados("Julio", " Profe", 5600);
		empleados.getDatosEmp();
		System.out.println("");
		
		System.out.println("Profesor:\n");
		Profesores profesores = new Profesores("Mario", " Gomez", 40506);
		profesores.getDatosPro();
		System.out.println("");
		
		System.out.println("Administrativo:\n");
		Administrativo administrativo = new Administrativo("Pamela"," Perez", 4300);
		administrativo.getDatosAdm();
		
	
	}

}
