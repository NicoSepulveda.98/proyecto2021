package Ejercicio1;

public class Libros extends Comunes {
	
	private String comunes_autor;
	private String comunes_titulo;
	private int comunes_anio;
	private int comunes_isbn;
	private int multas;
	private int dias;
	private boolean prestados;
	
	
	public Libros() {
		
		this.setComunes_autor(comunes_autor());
		this.setComunes_titulo(comunes_titulo());
		this.setComunes_anio(comunes_anio());
		this.setComunes_isbn(comunes_isbn());
		this.multas = 0;
		this.dias = 0;
		this.prestados = false;
	}


	public String getComunes_autor() {
		return comunes_autor;
	}


	public void setComunes_autor(String comunes_autor) {
		this.comunes_autor = comunes_autor;
	}


	public String getComunes_titulo() {
		return comunes_titulo;
	}


	public void setComunes_titulo(String comunes_titulo) {
		this.comunes_titulo = comunes_titulo;
	}
	
	public int getComunes_anio() {
		return comunes_anio;
	}


	public void setComunes_anio(int comunes_anio) {
		this.comunes_anio = comunes_anio;
	}


	public int getComunes_isbn() {
		return comunes_isbn;
	}


	public void setComunes_isbn(int comunes_isbn) {
		this.comunes_isbn = comunes_isbn;
	}


	public int getMultas() {
		return multas;
	}


	public void setMultas(int multas) {
		this.multas = multas;
	}


	public boolean getPrestados() {
		return prestados;
	}


	public void setPrestados(boolean prestados) {
		if (dias <= 5) {
			this.prestados = prestados;
		}
	}

	public void multa_diaria() {
		this.multas = multas + 1290;
		
	}
	
	public void muestra_libro() {
		System.out.println("> " + getComunes_titulo() + " , " + getComunes_autor()
		+ " , " + getComunes_anio() + ", ISBN: " + getComunes_isbn());
		
		if (getPrestados() == false) {
			System.out.println("Texto Prestado.");
		}
		
		else {
			System.out.println("Texto Disponible.");
		}
	}
}


