package Ejercicio1;
import java.util.Scanner;

public class Comunes {
	
	private String comunes_autor;
	private String comunes_titulo;
	private int comunes_anio;
	private int comunes_isbn;
	Scanner sc = new Scanner(System.in);
	
	
	public String comunes_autor() {
		
		System.out.println("> Por favor ingrese el nombre del autor: ");
		comunes_autor = sc.nextLine();
	
		return this.comunes_autor;	
	}
	
	public String comunes_titulo() {
		
		System.out.println("> Por favor ingrese el titulo: ");
		comunes_titulo = sc.nextLine();
		
		return this.comunes_autor;
	}
	
	public int comunes_anio() {
		
		System.out.println("> Ingrese el año de publicacion: ");
		comunes_anio = sc.nextInt();
		if (comunes_anio > 0 && comunes_anio <= 2021) {
			System.out.println(" Ha ingresado un año correcto");
		
		}
		
		else {
			System.out.println("¡Por favor, ingrese un año correcto!");
		}
		
		return this.comunes_anio;
	}
	
	public int comunes_isbn() {
		
		System.out.println("> Por favor ingresar el código ISBN del libro:\n"
				+ "En caso de ser una revista ingrese la Edición:\n");
		comunes_isbn = sc.nextInt();
		
		return this.comunes_isbn();
	}
}
