package Ejercicio1;
import java.util.Scanner;

public class Revistas extends Comunes {
	
	private String comunes_autor;
	private String comunes_titulo;
	private String genero;
	private int comunes_anio;
	private int comunes_isbn;
	private int arriendo;
	private boolean prestados;
	private boolean caducado;
	private int dia;
	Scanner sc = new Scanner(System.in);

	public Revistas() {
		
		this.setComunes_autor(comunes_autor());
		this.setComunes_titulo(comunes_titulo());
		this.setGenero(getGenero());
		this.setComunes_anio(comunes_anio());
		this.setComunes_isbn(comunes_isbn());
		this.setArriendo(getArriendo());
		this.prestados = false;
		this.setCaducado(false);
		this.dia = 0;
		
		
	}
	
	public String getComunes_autor() {
		return comunes_autor;
	}

	public void setComunes_autor(String comunes_autor) {
		this.comunes_autor = comunes_autor;
	}

	public String getComunes_titulo() {
		return comunes_titulo;
	}

	public void setComunes_titulo(String comunes_titulo) {
		this.comunes_titulo = comunes_titulo;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public int getComunes_anio() {
		return comunes_anio;
	}

	public void setComunes_anio(int comunes_anio) {
		this.comunes_anio = comunes_anio;
	}

	public int getComunes_isbn() {
		return comunes_isbn;
	}

	public void setComunes_isbn(int comunes_isbn) {
		this.comunes_isbn = comunes_isbn;
	}

	public int getArriendo() {
		return arriendo;
	}

	public void setArriendo(int arriendo) {
		this.arriendo = arriendo;
	}

	public boolean getPrestados() {
		return prestados;
	}

	public void setPrestados(boolean prestados) {
		if (dia < 5) {
		this.prestados = prestados;
		}
		
	}

	public boolean setCaducado() {
		return caducado;
	}

	public void setCaducado(boolean caducado) {
		this.caducado = caducado;
	}

	public int getDia() {
		return dia;
	}

	public void setDia(int dia) {
		this.dia = dia;
	}
	
	public String Genero() {
		System.out.println("¿Que genero es la revista?:\n"
				+ "Comic:\nCientifica:\nDeportiva:\nGastronomica:\n");
		genero = sc.nextLine();
		
		return this.genero;
	}
	
	public void muestra_revista() {
		System.out.println("> " + getComunes_titulo() + " , " + getComunes_autor()
		+ " , " + getComunes_anio() + ", ISBN: " + getComunes_isbn() + " , " + getGenero());
	}
	


}
