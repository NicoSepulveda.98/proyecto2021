package Ejercicio2;

public class Bingo {

	private int numeros;
	private int cartones;
	private int participantes;
	
	
	public int getNumeros() {
		return numeros;
	}
	public void setNumeros(int numeros) {
		this.numeros = numeros;
	}
	public int getCartones() {
		return cartones;
	}
	public void setCartones(int cartones) {
		this.cartones = cartones;
	}
	public int getParticipantes() {
		return participantes;
	}
	public void setParticipantes(int participantes) {
		this.participantes = participantes;
	}
	
}
