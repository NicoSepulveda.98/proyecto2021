package Ejercicio1;
import java.util.Iterator;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc = new Scanner(System.in);
		Estudiantes c = new Estudiantes();
		
		// Estudiantes año 2021
		c.Estudiante.add("Nico");
		c.Estudiante.add("Felipe");
		c.Estudiante.add("Pepe");
		c.Estudiante.add("Nicole");
		c.Estudiante.add("Minato");
		c.Estudiante.add("Miranda");
		
		
		Iterator<String> iterador = c.Estudiante.iterator();
		System.out.println("Estudiantes año 2021\n");
		while(iterador.hasNext()) {
			
			System.out.println("Estudiantes año 2021: " + iterador.next() + "\n");
		}
		
		System.out.println("Ingrese el nombre para consultar su existencia en los Estudiantes 2021: ");
		String estado_consulta_2021 = sc.nextLine();
		
		if (c.Estudiante.contains(estado_consulta_2021)) {
			System.out.println("¡Nombre encontrado!\n");
			}
		else {
			System.out.println("¡Nombre no encontrado!\n");
		}
		// Estudiantes año 2020
		c.Estudiante2020.add("Minato");
		c.Estudiante2020.add("Ricardo");
		c.Estudiante2020.add("Nicole");
		c.Estudiante2020.add("Esperanza");
		c.Estudiante2020.add("Juan");
		c.Estudiante2020.add("Nico");
		
		Iterator<String> iterador2020 = c.Estudiante2020.iterator();
		System.out.println("Estudiantes año 2020\n");
		while(iterador2020.hasNext()) {
			
			System.out.println("Estudiantes año 2020: " + iterador2020.next() + "\n");
		}
		System.out.println("Ingrese el nombre para consultar su existencia en los Estudiantes 2020: ");
		String estado_consulta_2020 = sc.nextLine();
		
		if (c.Estudiante2020.contains(estado_consulta_2020)) {
			System.out.println("¡Nombre encontrado!\n");
			}
		else {
			System.out.println("¡Nombre no encontrado!\n");
		}
		//Unir los elementos de los dos conjuntos
		c.Estudiante.addAll(c.Estudiante2020);
		System.out.println("Union de Estudiantes del año 2021 y 2020:\n");
		System.out.println(c.Estudiante);
		
	}

}
