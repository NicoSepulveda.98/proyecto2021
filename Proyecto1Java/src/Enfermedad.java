
public class Enfermedad {
	
	// Definición de atributos
    private double prob_infeccion;
    private int prom_pasos;
    
    
    // Constructor por parámetros
    public Enfermedad(double prob_infeccion, int prom_pasos) {
    	this.prob_infeccion = prob_infeccion;
    	this.prom_pasos = prom_pasos;
    }

    // Getter's

    public int getPromPasos() {
	
    	return this.prom_pasos;
    }

    public double getProbInfeccion() {
	
    	return this.prob_infeccion;
    }
    
    // Setter's

	public void setProbInfeccion(double s) {
	
    	this.prob_infeccion = s;
    }
    
    public void setProPasos(int prom_pasos) {
    	
    	this.prom_pasos = prom_pasos;
    }
    
}