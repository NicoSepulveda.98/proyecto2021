# Simulador de pandemias para análisis epidemiológico
El siguiente proyecto consiste en un programa que va orientado al la generación de un simulador
Donde nuestro principal objetivo es construir y crear un simulador, el cual representará una pandemia en x días.


# Historia:
Fue escrito y desarrollado en conjunto por los autores Jorge Carrillo Silva, Nicolás Sepúlveda Falcón y Simone Urrutia Loyola, utilizando para ello el lenguaje de programación Java, versión de openjdk 11 ~ 14
Se basa en la guía de estilo nativa del IDE Eclipse.
Contiene dos librerías, las cuales son:                                                                                                 
A.- java.util.Random                                                                                                                  B.- java.util.ArrayList 


# Para empezar:
Es requisito haber instalado Java previamente en su computadora, de lo contrario, el programa no podrá ser lanzado.
Recomendamos que el sistema operativo sobre el cual pretende lanzar el programa, corra sobre el kernel Linux (Debian, Ubuntu, Arch, entre otras.)


# Instalación:
Para instalar y ejecutar el programa en su máquina, es necesario que siga las presentes indicaciones:
1- Clonar el repositorio "Proyecto-Java-U1" en el directorio de su preferencia, el enlace HTTPS para clonar el programa es el siguiente:
https://gitlab.com/jorgecs-8895/proyecto-java-u1.git

2- Luego, deberá entrar en la carpeta clonada que lleva por nombre "proyecto-java-u1/src" que contiene 5 clases, llamadas Ciudadano, Comunidad, Enfermedad, Main y Simulador, respectivamente y todas con la extensión de programa ".java"

3- Posteriormente, ya está usted preparado para ejecutar el programa. Puede ser lanzado desde una consola de su IDE de preferencia o desde una terminal en Linux


# Codificación:
El programa soporta la codificación estándar UTF-8


# Construido con:
Eclipse: IDE utilizado por defecto para el desarrollo del proyecto.


# Licencia:
Este proyecto está sujeto bajo la Licencia GNU GPL v3. 


# Autores y creadores del proyecto:
Jorge Carrillo Silva, Nicolás Sepúlveda Falcón y Simone Urrutia Loyola.
