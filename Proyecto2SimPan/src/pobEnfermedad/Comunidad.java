package pobEnfermedad;
import java.util.Random;
import java.util.Collections;

import java.util.ArrayList;

public class Comunidad  {
	
	/* Definición de atributos 
	 * Instanciar en main
	 */
    private int nro_ciudadano;
    private ArrayList<Ciudadano> ciudadano = new ArrayList<Ciudadano>();
    private double promedio_conexion;
    private Enfermedad enfermedad;
    private int numero_infectados = 10;
    private double probabilidad_conexion;
    private double prob_asma;
    private double prob_obesidad;
    private int numero_contagios = numero_infectados;
    private int numero_inmune;



    /* Constructor por parámetros */
    public Comunidad(int nro_ciudadano, double promedio_conexion, Enfermedad enfermedad, int numero_infectados,
	    double probabilidad_conexion, double prob_asma, double prob_obesidad) {

    	this.nro_ciudadano = nro_ciudadano;
    	this.promedio_conexion = promedio_conexion;
    	this.enfermedad = enfermedad;
    	this.numero_infectados = numero_infectados;
    	this.probabilidad_conexion = probabilidad_conexion;
    	this.prob_asma = prob_asma;
    	this.prob_obesidad = prob_obesidad;
    
    	Random rand = new Random();
    	/* Metodo practico para crear la comunidad con su respectiva ID y Edad*/
    	for (int i = 0; i < nro_ciudadano; i++) {
    		this.ciudadano.add(new Ciudadano(i+1, rand.nextInt(80)+1));	
    	}
    
		int aux = 0;
		int seleccionados;
	
		/* 10 primero contagiados */
		while (aux < numero_infectados) {
			seleccionados = rand.nextInt(this.ciudadano.size());
		    if (!this.ciudadano.get(seleccionados).getInfectado()) {
		    	this.ciudadano.get(seleccionados).setInfectado(true);
		    	aux++;
		    }
		}		
    	/* 25% de la poblacion con asma */
    	int aux2 = 0;
    	int total_asma;
    	int seleccion_asma;
    		
    	total_asma = (int) Math.round(this.getNro_ciudadano() * this.getProbabilidadAsma());
    	while(aux2 < total_asma) {
    		seleccion_asma = rand.nextInt(this.ciudadano.size());
    		if(!this.ciudadano.get(seleccion_asma).getAsma()){
    			this.ciudadano.get(seleccion_asma).setAsma(true);
    			aux2 ++;		
    		}		
    	}
    	/* 65% de las personas con obesidad */
    	int aux3 = 0;
    	int total_obesidad;
    	int seleccion_obesidad;
    		
    	total_obesidad = (int) Math.round(this.getNro_ciudadano() * this.getProbabilidadObesidad());
    	while(aux3 < total_obesidad) {
    		seleccion_obesidad = rand.nextInt(this.ciudadano.size());
    		if(!this.ciudadano.get(seleccion_obesidad).getObesidad()){
    			this.ciudadano.get(seleccion_obesidad).setObesidad(true);
    			aux3 ++;
    		}
    	}
    }	
 
    /* Getter's */
    public int getNro_ciudadano() {
	
    	return this.nro_ciudadano;
    }

    public ArrayList<Ciudadano> getCiudadano() {
	
    	return this.ciudadano;
    }

    public double getPromedio_conexion() {

    	return this.promedio_conexion;
    }

    public Enfermedad getEnfermedad() {
	
    	return this.enfermedad;
    }

    public int getNumero_infectados() {
	
    	return this.numero_infectados;
    }

    public double getProbabilidad_conexion() {
	
    	return this.probabilidad_conexion;
    }
    
    public double getProbabilidadAsma() {
		
    	return this.prob_asma;
	}
    
    public double getProbabilidadObesidad() {
		
    	return this.prob_obesidad;
	}
    
    public int getNumero_contagios() {
		
    	return numero_contagios;
	}

	public int getNumero_inmune() {
		
		return numero_inmune;
	}

    /* Setter's */
    public void setNro_ciudadano(int nro_ciudadano) {
	
    	this.nro_ciudadano = nro_ciudadano;
    }

    public void setProbabilidad_conexion(double probabilidad_conexion) {
	
    	this.probabilidad_conexion = probabilidad_conexion;
    }

    public void setCiudadano(ArrayList<Ciudadano> ciudadano) {
	
    	this.ciudadano = ciudadano;
    }

    public void setPromedio_conexion(double promedio_conexion) {
    	this.promedio_conexion = promedio_conexion;
    }

    public void setNumero_infectados(int numero_infectados) {
	
    	this.numero_infectados = numero_infectados;
    }

    public void setEnfermedad(Enfermedad enfermedad) {
	
    	this.enfermedad = enfermedad;
    }
    public void setProbabilidadAsma(double asma) {
		
    	this.prob_asma = asma;
	}

	public void setProbabilidadObesidad(double obesidad) {
		
		this.prob_obesidad = obesidad;
	}

	public void setNumero_contagios() {
		
		this.numero_contagios = this.numero_contagios + 1;
	}

	public void setNumero_inmune() {
		
		this.numero_inmune = this.numero_inmune + 1;
	}
}
 