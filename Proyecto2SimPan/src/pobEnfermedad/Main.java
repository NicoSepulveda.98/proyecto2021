package pobEnfermedad;



public class Main {
	// Definimos Main, como el inicializador de las clases y el código en general.

	public static void main(String[] args) {

		// Variables constructoras.
		Enfermedad enfermedad = new Enfermedad(0.3, 18);
		Comunidad comunidad = new Comunidad(1000, 5, enfermedad, 10, 0.6, 0.25, 0.65);
		Simulador simulador = new Simulador(comunidad, enfermedad);
		simulador.simular(51);
	}
}