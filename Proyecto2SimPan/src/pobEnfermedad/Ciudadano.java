package pobEnfermedad;

public class Ciudadano {
	
	/* Definición de atributos */
	private int id;
    private boolean infectado;
    private boolean estado;
    private int contador;    
    private boolean inmune;
    private int sanos;
    private boolean asma;
    private boolean obesidad;
    private boolean grave;
    private int edad;
    private int dosis1_AstraZeneca=0;
    private int dosis2_Sinovac=0;
    private int dosis3_CanSino =0;
   
    /* Constructor por parámetros */
    Ciudadano(int id, int edad) {
    	
    	this.id = id;
    	this.infectado = false;
    	this.estado = false;
    	this.contador = 0;
    	this.inmune = false;
    	this.asma = false;
    	this.obesidad = false;
    	this.grave = false;
    	this.edad = edad;
    }

    /* Getter's */
    
    public int getId() {
	
    	return this.id;
    }

    public boolean getEstado() {
	
    	return this.estado;
    }

    public int getContador() {
    	
    	return this.contador;
    }
    
    public boolean getInfectado() {
	
    	return this.infectado;

    }
    
    public boolean getInmune() {
    	
    	return this.inmune;
    }
    
    public int getSanos() {
	
    	return this.sanos;
    }
    
    public boolean getAsma() {
    	
    	return this.asma;
    }

    public boolean getObesidad() {
    	
    	return this.obesidad;
    }
  
    public boolean getGrave() {
    	
    	return this.grave;
    }
    
    public int getEdad() {
    	
    	return this.edad;
    }
    
    public int getDosis1_AstraZeneca() {
		
    	return dosis1_AstraZeneca;
	}
    
    public int getDosis2_Sinovac() {
		
    	return dosis2_Sinovac;
	}
    
    public int getDosis3_CanSino() {
		
    	return dosis3_CanSino;
	}
    
    /* Setter's */

    public void setId(int id) {
	
    	this.id = id;
    }

    public void setInfectado(boolean infectado) {
	
    	this.infectado = infectado;
    }

    public void setEstado(boolean estado) {
	
    	this.estado = estado;
    }
    
    public void setContador(int contador) {
    	this.contador = contador;
    }
    
    public void setInmune(boolean inmune) {
    	this.inmune = inmune;
    }

    public void setSanos(int sanos) {
	
    	this.sanos = sanos;
    }
    
    public void setAsma(boolean asma) {
    	
    	this.asma = asma;
    }
    
    public void setObesidad(boolean obesidad) {
    	
    	this.obesidad = obesidad;
    }
    
    public void setGrave(boolean grave) {
    	
    	this.grave = grave;
    }

    public void setEdad(int edad) {
    	
    	this.edad = edad;
    }

	public void setDosis1_AstraZeneca(int dosis1_AstraZeneca) {
		
		this.dosis1_AstraZeneca = dosis1_AstraZeneca;
	}

	public void setDosis2_Sinovac(int dosis2_Sinovac) {
		
		this.dosis2_Sinovac = dosis2_Sinovac;
	}

	public void setDosis3_CanSino(int dosis3_CanSino) {
		
		this.dosis3_CanSino = dosis3_CanSino;
	}
}