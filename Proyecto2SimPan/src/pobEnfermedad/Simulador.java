package pobEnfermedad;

import vacunas.Sinovac;
import vacunas.AstraZeneca;
import vacunas.CanSino;
import java.util.Random;
//import java.util.Collections;

public class Simulador {
	
	/* Definición de atributos y objetos. */
    private Comunidad comunidad;
    private Enfermedad enfermedad;
    private Ciudadano ciudadano;

    /* Generar constructores dentro de la clase */
    AstraZeneca astrazeneca = new AstraZeneca();
    CanSino cansino = new CanSino();
    Sinovac sinovac = new Sinovac();
    
    /* Método Simulador */
    public Simulador(Comunidad comunidad, Enfermedad enfermedad) {
    	
    	this.comunidad = comunidad;
    	this.enfermedad = enfermedad;

    }
	/* Simulación de días para la pandemía */
    public void simular(int numero_dias) {
    	
    	impresion_inicial(ciudadano);
    	System.out.println("\n\t-----------------------------------------\n");
    	for (int i = 1; i < numero_dias; i++) {
    		System.out.println("  Dia:" +i);
    		imprimir_contagiados(ciudadano);
    		contagio_contacto();

    		if (i == 3 || i == 6) {
    			Vacunacion(i);
    		}
    	}
    	salida_final(ciudadano);
    }
    
	public void contagio_contacto() {
		
		/* Se genera valor aleatorio para la probabilidad. */
		Random rand = new Random();
		double aumento_gravedad = 0.9;
		double efecto_vacuna = 1;
	
		for (Ciudadano p : this.comunidad.getCiudadano()) {
			if(!verificar_muerte(p)){
				if(!verificar_inmune(p)){
					if(!p.getInfectado()){
						/* Implementar efecto vacuna 1*/
						if (p.getDosis1_AstraZeneca() == astrazeneca.getNumeroDosis()) {
							efecto_vacuna = astrazeneca.getVarianza();
						}
						/* Se hace la "ecuacion" para infectar al ciudadano. */
						if (rand.nextDouble() < (0.3 * 0.6 * efecto_vacuna)) {	    							
							p.setInfectado(true);							
							comunidad.setNumero_contagios();
							/* Verificar si es grave por afeccion y enfermedad */ 
							if (p.getAsma() == true && p.getObesidad() == true) {
								p.setGrave(true);
							}
						}
					}
					else {
						/* Quitar gravedad al Ciudadano */
						if (p.getDosis2_Sinovac() == sinovac.getNumeroDosis()) {
							p.setGrave(false);
						}
		
						/* Aumenta su probabilidad de morir segun su condicion
						 * Asmatico = 60%
						 * Obesidad = 50%
						 * Ambos = 70%
						 */
						
						if (p.getGrave() == true) {
							if(p.getAsma() == true && p.getObesidad() == false) {
								aumento_gravedad = 0.4;
							}
							if(p.getObesidad() == true && p.getAsma() == false) {
								aumento_gravedad = 0.5;
							}
							if(p.getAsma() == true && p.getObesidad() == true) {
								aumento_gravedad = 0.3;
							}
						}
						else {
							aumento_gravedad = 0.9;
						}
						if(verificar_pasos(p, enfermedad.getPromPasos())) {
							if (rand.nextDouble() < aumento_gravedad) {
								p.setInfectado(false);
								p.setInmune(true);
								p.setContador(0);
								comunidad.setNumero_inmune();
							}
							else {
								/* Sin probabilidad de morir */
								if (p.getDosis2_Sinovac() == sinovac.getNumeroDosis()) {
								}
								else {
									p.setEstado(true);
									p.setInfectado(false);
									p.setInmune(true);
									p.setContador(0);	
								}
							}
						}
						else {
							int contador = p.getContador();
							p.setContador(contador + 1);
						}
					}
				}
			}
		}	
	}
	
	private boolean verificar_pasos(Ciudadano p, int promPasos) {
		
		int pasos = p.getContador();
		
		if(pasos == promPasos) {
		
			return true;
		}
		else {
		
			return false;
		}	
	}

	private boolean verificar_inmune(Ciudadano p) {
		
		/* Verificar si es inmune con la tercera vacuna */
		if (p.getDosis3_CanSino() == cansino.getNumeroDosis()) {
			if (p.getInfectado()) {
				p.setInfectado(false);
				comunidad.setNumero_inmune();
			}
			p.setInmune(true);
		}
		if(p.getInmune()) {
		
			return true;
		}
		else {
		
			return false;
		}	
	}

	private boolean verificar_muerte(Ciudadano p) {
		
		if(p.getEstado()) {
		
			return true;
		}
		else {
		
			return false;
		}
		
	}
	public void Vacunacion(int pasos) {
		
		//Collections.shuffle(comunidad.getCiudadano());
		int poblacion_vacunable = Math.round(comunidad.getNro_ciudadano() / 2);
		int poblacion_vacuna1 = Math.round((poblacion_vacunable * 9) / 18);
		int poblacion_vacuna2 = Math.round((poblacion_vacunable * 6) / 18);
		int poblacion_vacuna3 = Math.round((poblacion_vacunable * 3) / 18);
		int i  = 0;
		int poblacion_final2 = poblacion_vacuna1 + poblacion_vacuna2;
		int poblacion_final3 = poblacion_vacuna1 + poblacion_vacuna2 + poblacion_vacuna3;
		
		for (i = 0; i < comunidad.getCiudadano().size(); i++) {
			if (i <= poblacion_vacuna1) {
				comunidad.getCiudadano().get(i).setDosis1_AstraZeneca(1);
			}
			else if ((i > poblacion_vacuna1) && (i <= poblacion_final2)) {
				comunidad.getCiudadano().get(i).setDosis2_Sinovac(1);
			}
			else if ((i > (poblacion_final2)) && (i <= (poblacion_final3))) {
				comunidad.getCiudadano().get(i).setDosis3_CanSino(1);
			}
		}
		i = 0;
		if (pasos == 6) {
		
			for (i = 0; i < comunidad.getCiudadano().size(); i++) {
				if (comunidad.getCiudadano().get(i).getDosis1_AstraZeneca() == 1 && astrazeneca.getNumeroDosis() == 2) {
					comunidad.getCiudadano().get(i).setDosis1_AstraZeneca(2);
				}
				else if (comunidad.getCiudadano().get(i).getDosis2_Sinovac() == 1 && sinovac.getNumeroDosis() == 2) {
					comunidad.getCiudadano().get(i).setDosis2_Sinovac(2);
				}
				else if (comunidad.getCiudadano().get(i).getDosis3_CanSino() == 1 && cansino.getNumeroDosis() == 2) {
					comunidad.getCiudadano().get(i).setDosis3_CanSino(2);
				}	
			}
		}
	}

	/* Salida por pantalla del programa hasta simular los dias */
	public void imprimir_contagiados(Ciudadano p) {
		
		int cont_contagiado = 0;
		
		for (Ciudadano c : comunidad.getCiudadano()) {
			if(c.getInfectado()) {
				cont_contagiado ++;
			}	
		}
		System.out.println("\tEl total de contagio(s) de la Comunidad es de: " + comunidad.getNumero_contagios() + ", " 
		+ "Con: " + cont_contagiado + " Caso(s) Activo(s)");
		System.out.println("");	
	}
	
	/* Imprimir datos generales al iniciar la simulacion */
	public void impresion_inicial(Ciudadano p) {
		
		System.out.println("\r\n\t\t**** ¡DATOS GENERALES DE LA POBLACION! ****\n");
		System.out.println("  > N° Habitantes o Total de la Poblacion: " + comunidad.getNro_ciudadano() + " Personas");
		System.out.println("  > Infectados Iniciales: " + comunidad.getNumero_infectados() + " Personas");
		System.out.println("  > Poblacion con Afección: " + contar_afeccion_base() +  " Personas" +
				"\n  > Poblacion con Enfermedad Base: " + contar_enfermedad_base() + " Personas");
		System.out.println("  > El Promedio de Edad de la Comunidad es: " + promedio_edad() + " Años");
		numero_vacunas();
	}
	
	public int contar_afeccion_base() {
		
		int contador = 0;
		
		for (Ciudadano c: comunidad.getCiudadano()) {
			if (c.getObesidad()) {
				contador ++;
			}
		}
		return contador;
	}
	
	public int contar_enfermedad_base() {
		
		int contador = 0;
		
		for (Ciudadano c: comunidad.getCiudadano()) {
			if (c.getAsma()) {
				contador ++;
			}	
		}
		return contador;
	}
	
	public double promedio_edad() {
		
		double promedio = 0;
		
		for (Ciudadano c: comunidad.getCiudadano()) {
			promedio = promedio + c.getEdad();
		}
		return Math.round(promedio/comunidad.getNro_ciudadano());
	}
	
	public void numero_vacunas() {
		
		int poblacion_vacunable = Math.round(comunidad.getNro_ciudadano() / 2);
		int disponible_vacuna1 = Math.round((poblacion_vacunable * 9) / 18);
		int disponible_vacuna2 = Math.round((poblacion_vacunable * 6) / 18);
		int disponible_vacuna3 = Math.round((poblacion_vacunable * 3) / 18);
		
		System.out.println("\n  **¡Vacunas disponibles para la Poblacion!**\n" +
		"\n  > Vacuna (1) AstraZeneca: " + disponible_vacuna1 + " Disponibles" +
		"\n  > Vacuna (2) Sinovac: " + disponible_vacuna2 + " Disponibles" +
		"\n  > Vacuna (3) Cansino: " + disponible_vacuna3 + " Disponibles");
	}
	
	/* Imprimir la salida final */
	public void salida_final(Ciudadano p) {
		
		System.out.println("\n\t-----------------------------------------\n");
		System.out.println("\n\t**** ¡Datos Generales de la Poblacion al Finalizar la Simulacion! ****\n");
		System.out.println("  > N° Habitantes o Total de la Poblacion Final: " + poblacion_final() + " Personas");
		System.out.println("  > Infectados finales: " + contagiados_final() + " Personas");
		System.out.println("  > Total de Muertos: " + muertos_finales());
		System.out.println("  > Personas Vacunadas: " + vacunados() + " Vivas");
		System.out.println("  > Personas Inmunes: " + comunidad.getNumero_inmune());
		System.out.println("  > Promedio de Edad de la Poblacion Viva: " + promedio_edad_viva());
	}
	
	/* Calcular poblacion final */
	public int poblacion_final() {
		 
		 int contador = 0;
		 
		 for(Ciudadano c: this.comunidad.getCiudadano()) {
			 if(c.getEstado()){
				 contador ++;
			 }
		 }
		 
		 return comunidad.getNro_ciudadano() - contador;
	 }
	
	/* Contagiados finales de la simulacion */
	public int contagiados_final() {
		
		int contador = 0;
		
		for(Ciudadano c: this.comunidad.getCiudadano()) {
			if (c.getInfectado()) {
				contador ++;
			}
		}
		return contador;
	}
	/* Calcular muertos finales */
	public int muertos_finales() {
		
		int contador = 0;
		
		for(Ciudadano c: this.comunidad.getCiudadano()) {
			if (c.getEstado()) {
				contador ++;
			}
		}
		return contador;
	}
	
	/* Calcular los vacunados*/
	public int vacunados() {
		
		int contador = 0;

		for(int i = 0; i < this.comunidad.getCiudadano().size(); i++) {
			if (!comunidad.getCiudadano().get(i).getEstado()) {
				if (comunidad.getCiudadano().get(i).getDosis1_AstraZeneca() == astrazeneca.getNumeroDosis()) {
					contador ++;
				}
				if (comunidad.getCiudadano().get(i).getDosis2_Sinovac() == sinovac.getNumeroDosis()) {
					contador ++;
				}
				if (comunidad.getCiudadano().get(i).getDosis3_CanSino() == cansino.getNumeroDosis()) {
					contador ++;		
				}
			}
		}
		return contador;
	}
	
	public double promedio_edad_viva() {

		double suma = 0;
		double vivos = 0;
		
		for (Ciudadano c: comunidad.getCiudadano()) {
			if (!c.getEstado()) {
				suma = suma + c.getEdad();
			}
		}
		
		for (Ciudadano p: comunidad.getCiudadano()) {
			if (!p.getEstado()) {
				vivos ++;
			}
		}
		return Math.round(suma/vivos);
	}
}
	