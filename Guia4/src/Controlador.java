import java.util.ArrayList;
import java.util.Scanner;
import java.util.Random;

public class Controlador {

	Controlador(){
		
		Automovil a = new Automovil();
		
		/* valor por defecto */
		String cilindrada = "1.2";
		
		/* dos formas distintas de instanciar */
		//Motor m = new Motor(cilindrada);
		Motor m1 = new Motor();
		a.setMotor(m1);
		
		Velocimetro v = new Velocimetro();
		a.setVelocimetro(v);
		
		Estanque e = new Estanque();
		a.setEstanque(e);
		
		for (int i=0; i<4;i++) {
			a.setRueda(new Rueda());
		}
		
		//a.chequeo_ruedas();
		a.reporte_automovil();
		System.out.println("-------------------");
		a.enciende_automovil();
	
		/* Comienza la simulación*/
		Scanner scan = new Scanner(System.in);
		Random rd = new Random();
		
		while(a.getEstanque().getVolumen() > 0.0) {
			if(a.getEncendido() == true) {
				System.out.println("Automovil encendido");
				System.out.println("Presione para mover la tecla 1: ");
				int tecla = scan.nextInt();
				if (tecla == 1) {
					int movimiento = rd.nextInt(10) + 1;
					v.setVelocidad(movimiento);
					int tiempo = rd.nextInt(10) + 1;
					double distancia = a.getVelocimetro().getVelocidad() * tiempo;
					double consumo = a.getEstanque().getVolumen();
					double consumo_f = consumo * distancia;
					a.getEstanque().setVolumen(consumo_f);
					
					for (Rueda r: a.getRueda()) {
						int desgaste = rd.nextInt(10) + 1;
						r.setDesgaste(desgaste);
								
					}
					a.reporte_automovil();
					a.chequeo_ruedas();
					
			
				}
				else {
					System.out.println("Por favor ingresar tecla valida");
				}
				
			}
			
		}
			System.out.println("Se ha quedado sin combustible, a de proceder apagar su vehiculo");
			a.apagar();
		
		
		
	}


}
