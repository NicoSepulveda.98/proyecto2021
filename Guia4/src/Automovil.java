import java.util.ArrayList;
import java.util.Random;


public class Automovil {
	private Motor motor;
	private Velocimetro velocimetro;
	private ArrayList<Rueda> rueda;
	private Estanque estanque;
	private Boolean encendido;
	private int durabilidad = 100;
	Random rand = new Random();
	private int distancia = 0;
	
	
	
	Automovil(){
		this.rueda = new ArrayList<Rueda>();
		this.encendido = false;

		
	}
	
	
	public Motor getMotor() {
		return motor;
	}
	
	public void setMotor(Motor motor) {
		this.motor = motor;
	}

	public Velocimetro getVelocimetro() {
		return velocimetro;
	}

	public void setVelocimetro(Velocimetro velocimetro) {
		this.velocimetro = velocimetro;
	}

	public Estanque getEstanque() {
		return estanque;
	}

	public void setEstanque(Estanque estanque) {
		this.estanque = estanque;
	}

	public Boolean getEncendido() {
		return encendido;
	}

	public void setEncendido(Boolean encendido) {
		this.encendido = encendido;
	}

	public ArrayList<Rueda> getRueda() {
		return rueda;
	}

	public void setRueda(Rueda rueda) {
		this.rueda.add(rueda);
	}
	
	public void enciende_automovil() {
		
		this.encendido = true;
		estanque.setVolumen((estanque.getVolumen()/durabilidad) * 1.4);
		
		
	}
	
	public void movimiento() {
	
		System.out.println("El auto se mueve");
	}
	
	public void apagar() {
		this.encendido = false;
	}
	public void chequeo_ruedas() {
	
		for (Rueda r : this.rueda) {
			System.out.println(r.getDesgaste());
		}
		for (int i = 0; i < 4; i ++) {
			if(rueda.get(i).getDesgaste() <= 5) {
				System.out.println("Es necesario modificar la rueda " + i+1);
				System.out.println("Es necesario modificar el neumativo, aguarde unos segundos..." +
				"Apagaremos su vehiculo");
				apagar();
				System.out.println("..." + "\n..." + "\n..." + "\n Neumatico(s) mofidicado(s).");
				enciende_automovil();
			}
		}
	}
	
	public void recorrido(int km) {
		
		if(velocimetro.getVelocidad() + km >= velocimetro.getVelocidadMaxima()) {
			System.out.println("Superado el limite de velocidad");
			
		}
		else {
			velocimetro.setVelocidad(velocimetro.getVelocidad() + km);
		}
	}
	
	
	public void reporte_automovil() {
		System.out.println("El automovil tiene un motor de " + this.motor.getCilindrada());
		System.out.println("El estado actual del estanque es: " + this.estanque.getVolumen());
		System.out.println("La velocidad actual del automovil es " + this.velocimetro.getVelocidad());
		System.out.println("Ha recorrido: " + velocimetro.getVelocidad() + "Km");
		if(this.encendido == true) {
			System.out.println("Automovil apagado");
			
		}
		else {
			System.out.println("Automovil encendido");
		}
		System.out.println("Estado actual de las ruedas: ");
		chequeo_ruedas();
	}


}
