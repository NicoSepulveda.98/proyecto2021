
public class Velocimetro {
	
	private int velocidad;
	private int velocidad_maxima = 120;
	
	
	public int getVelocidad() {
		return velocidad;
	}
	
	public void setVelocidad(int velocidad) {
		this.velocidad = velocidad;
	}
	
	public int getVelocidadMaxima() {
		return this.velocidad_maxima;
	}


}
