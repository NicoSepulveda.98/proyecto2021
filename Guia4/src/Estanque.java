
public class Estanque {
	
	private Double volumen = 32.0;

	public Double getVolumen() {
		return volumen;
	}

	public void setVolumen(Double volumen) {
		this.volumen = volumen;
	}

}
