# El automovil
El siguiente proyecto consiste en un programa que va orientado al la generación de un simulador
Donde nuestro principal objetivo es construir y crear un simulador, el cual representará un automovil de carrera.


# Historia:
Fue escrito y desarrollado por Nicolás Sepúlveda Falcón utilizando para ello el lenguaje de programación Java, versión de openjdk 11 ~ 14
Se basa en la guía de estilo nativa del IDE Eclipse.
Contiene dos librerías, las cuales son:                                                                                                 
A.- java.util.Random                                                                                                                  B.- java.util.ArrayList 


# Para empezar:
Es requisito haber instalado Java previamente en su computadora, de lo contrario, el programa no podrá ser lanzado.
Recomendamos que el sistema operativo sobre el cual pretende lanzar el programa, corra sobre el kernel Linux (Debian, Ubuntu, Arch, entre otras.)


# Codificación:
El programa soporta la codificación estándar UTF-8


# Construido con:
Eclipse: IDE utilizado por defecto para el desarrollo del proyecto.


# Licencia:
Este proyecto está sujeto bajo la Licencia GNU GPL v3. 


# Autor y creador del proyecto:
Nicolás Sepúlveda Falcón 
