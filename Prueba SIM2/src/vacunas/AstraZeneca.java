package vacunas;

public class AstraZeneca implements IntVacunas {

	private int numero_dosis=2;
	private double varianza = 1.25;

	public int getNumeroDosis() {
		
		return this.numero_dosis;
	}


	public void setNumeroDosis() {
		
		this.numero_dosis = 2;
		
	}
	
	public double getVarianza() {
		
		return this.varianza;
	}

 
}
