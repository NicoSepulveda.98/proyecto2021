package pobEnfermedad;

import vacunas.Sinovac;
import vacunas.AstraZeneca;
import vacunas.CanSino;
import java.util.Random;

public class Simulador {
	
	/* Definición de atributos y objetos. */
    private Comunidad comunidad;
    private Enfermedad enfermedad;
    private Ciudadano ciudadano;

    /* Generar constructores dentro de la clase */
    AstraZeneca astrazeneca = new AstraZeneca();
    CanSino cansino = new CanSino();
    Sinovac sinovac = new Sinovac();
    
    /* Método Simulador */
    public Simulador(Comunidad comunidad, Enfermedad enfermedad) {
    	
    	this.comunidad = comunidad;
    	this.enfermedad = enfermedad;

    }
	/* Simulación de días para la pandemía */
    public void simular(int numero_dias) {
    	
    	impresion_inicial(ciudadano);
    	System.out.println("------------------------");
    	for (int i = 1; i < numero_dias; i++) {
    		imprimir_contagiados(ciudadano);
    		contagio_contacto();
    		
//    		if (i == 3 || i == 6) {
//    			Vacunacion(i);
//    			System.out.println(i);
//    		}
    		Vacunacion(i);
    		System.out.println(i);
    	}
    }
    
	public void contagio_contacto() {
		/* Se genera valor aleatorio para la probabilidad. */
		Random rand = new Random();
		double aumento_gravedad = 0.9;
		double efecto_vacuna = 1;
	
		for (Ciudadano p : this.comunidad.getCiudadano()) {
			if(!verificar_muerte(p)){
	
				if(!verificar_inmune(p)){
					
					if(!p.getInfectado()){
	
						/* Implementar efecto vacuna 1*/
						if (p.getDosis1_AstraZeneca() == astrazeneca.getNumeroDosis()) {
							efecto_vacuna = astrazeneca.getVarianza();
						}
						/* Se hace la "ecuacion" para infectar al ciudadano. */
						if (rand.nextDouble() < (0.3 * 0.9 * efecto_vacuna)) {	    							
							p.setInfectado(true);							
							comunidad.setNumero_contagios();
							/* Verificar si es grave por afeccion y enfermedad */ // CAMBIADO DE O A Y 
							if (p.getAsma() == true && p.getObesidad() == true) {
								p.setGrave(true);
								
							}
						}
					}
					else {
						/* Quitarle lo grave */
						if (p.getDosis2_Sinovac() == sinovac.getNumeroDosis()) {
							p.setGrave(false);
						}
					
						/* Aumenta su probabilidad de morir segun su condicion
						 * Asmatico = 60%
						 * Obesidad = 50%
						 * Ambos = 70%
						 * Comprobamos ya que dos personas pueden tener la enfermedad y la afeccion COMENTARIO NICO
						 */
						
						if (p.getGrave() == true) {
							if(p.getAsma() == true && p.getObesidad() == false) {
								aumento_gravedad = 0.4;
							}
							if(p.getObesidad() == true && p.getAsma() == false) {
								aumento_gravedad = 0.5;
							}
							if(p.getAsma() == true && p.getObesidad() == true) {
								aumento_gravedad = 0.3;
							}
						}
						else {
							aumento_gravedad = 0.9;
						}
						
						if(verificar_pasos(p, enfermedad.getPromPasos())) {
							// CAMBIADO Establecer probabilidad de muerte segun su gravedad MODIFICAR ESTE ELSE
							if (rand.nextDouble() < aumento_gravedad) {
								p.setInfectado(false);
								p.setInmune(true);
								p.setContador(0);
								comunidad.setNumero_inmune();
							
							}
							else {
							
								/* Si tiene dosis no muere */
								if (p.getDosis2_Sinovac() == sinovac.getNumeroDosis()) {
									
								}
								else {
									p.setEstado(true);
									p.setInfectado(false);
									p.setInmune(true);
									p.setContador(0);
									
								}
							}
							
							
						}
						else {
							int contador = p.getContador();
							p.setContador(contador + 1);
							
						}
					}
				}
			}
		}	
	}
	
	private boolean verificar_pasos(Ciudadano p, int promPasos) {
		
		int pasos = p.getContador();
		
		if(pasos == promPasos) {
		
			return true;
		}
		else {
		
			return false;
		}
		
	}
	// Chequeo del ciudadano para verificar si puede contagiarse o no
	private boolean verificar_inmune(Ciudadano p) {
		/* Verificar si es inmune con la tercera vacuna */
		//System.out.println(p.getDosis3_CanSino());
		//System.out.println(cansino.getNumeroDosis());
		//System.out.println(p.getInmune());
		if (p.getDosis3_CanSino() == cansino.getNumeroDosis()) {
			if (p.getInfectado()) {
				p.setInfectado(false);
				comunidad.setNumero_inmune();
			}
			p.setInmune(true);
		}
		//System.out.println(p.getInmune());
		if(p.getInmune()) {
		
			return true;
		}
		else {
		
			return false;
		}
		
	}

	private boolean verificar_muerte(Ciudadano p) {
		
		if(p.getEstado()) {
		
			return true;
		}
		else {
		
			return false;
		}
		
	}
	
	public void Vacunacion(int pasos) {
	
		Random rd = new Random();
		int poblacion_vacunable = Math.round(comunidad.getNro_ciudadano() / 2);
		int poblacion_vacuna1 = Math.round((poblacion_vacunable * 9) / 18);
		int poblacion_vacuna2 = Math.round((poblacion_vacunable * 6) / 18);
		int poblacion_vacuna3 = Math.round((poblacion_vacunable * 3) / 18);
		int i = 0;
		
		for (Ciudadano p : this.comunidad.getCiudadano()) {
			/* Se prueba la vacuna 1 que esta difinida para 2 dosis */
			while(i < poblacion_vacuna1) {
				int seleccion = rd.nextInt(this.comunidad.getCiudadano().size());
				if (pasos == 3) {
					if(this.comunidad.getCiudadano().get(seleccion).getDosis1_AstraZeneca() == 0 &&		
							this.comunidad.getCiudadano().get(seleccion).getDosis2_Sinovac() == 0 &&
							this.comunidad.getCiudadano().get(seleccion).getDosis3_CanSino() == 0) {
						
						this.comunidad.getCiudadano().get(seleccion).setDosis1_AstraZeneca(1);
						i++;
					}
				}
				if (pasos == 6 && astrazeneca.getNumeroDosis() == 2) {
					if(this.comunidad.getCiudadano().get(seleccion).getDosis1_AstraZeneca() == 1) {
						this.comunidad.getCiudadano().get(seleccion).setDosis1_AstraZeneca(2);
						i++;
					}
				}
			}
			i = 0;
			
			/* Las vacunas Sinovac y CanSino estan definidas para una dosis
			 * Sin embargo se crean condiciones si en la clase de cada unas se asigna a tener
			 * dos dosis.
			 */
			
			/* Se prueba la vacuna 2 la que esta definida por 1 dosis */
			while(i < poblacion_vacuna2) {
				int seleccion2 = rd.nextInt(this.comunidad.getCiudadano().size());
				if (pasos == 3) {
					if(this.comunidad.getCiudadano().get(seleccion2).getDosis1_AstraZeneca() == 0 &&		
							this.comunidad.getCiudadano().get(seleccion2).getDosis2_Sinovac() == 0 &&
							this.comunidad.getCiudadano().get(seleccion2).getDosis3_CanSino() == 0) {
						
						this.comunidad.getCiudadano().get(seleccion2).setDosis2_Sinovac(1);
						i++;
					}
				}
				if (pasos == 6 && sinovac.getNumeroDosis() == 2) {
					if(this.comunidad.getCiudadano().get(seleccion2).getDosis2_Sinovac() == 1) {
						this.comunidad.getCiudadano().get(seleccion2).setDosis2_Sinovac(2);
						i++;
					}
				}
			}
			i = 0;
			while (i < poblacion_vacuna3) {
				int seleccion3 = rd.nextInt(this.comunidad.getCiudadano().size());
				if (pasos == 3) {
					if(this.comunidad.getCiudadano().get(seleccion3).getDosis1_AstraZeneca() == 0 &&		
							this.comunidad.getCiudadano().get(seleccion3).getDosis2_Sinovac() == 0 &&
							this.comunidad.getCiudadano().get(seleccion3).getDosis3_CanSino() == 0) {
						
						this.comunidad.getCiudadano().get(seleccion3).setDosis3_CanSino(1);
						i++;
					}
				}
				if (pasos == 6 && cansino.getNumeroDosis() == 2) {
					if(this.comunidad.getCiudadano().get(seleccion3).getDosis3_CanSino() == 1) {
						this.comunidad.getCiudadano().get(seleccion3).setDosis3_CanSino(2);
						i++;
					}
				}
			}
		}
	}
	
	
	// Salida por pantalla del programa
	public void imprimir_contagiados(Ciudadano p) {
		
		int cont_contagiado = 0;
		
		for (Ciudadano c : comunidad.getCiudadano()) {
			if(c.getInfectado()) {
				cont_contagiado ++;
			}	
		}
		System.out.println("El total de contagiados de la Comunidad 1 es de: " + comunidad.getNumero_contagios() + ", " 
		+ "Casos Activos: " + cont_contagiado);
		System.out.println("");
		
	}
	
	
	public void impresion_inicial(Ciudadano p) {
		
		System.out.println("¡IMPRESION INICIAL DATOS GENERALES DE LA POBLACION!\n");
		System.out.println(">N° Habitantes o Total poblacion: " + comunidad.getNro_ciudadano());
		System.out.println(">Infectados iniciales: " + comunidad.getNumero_infectados());
		System.out.println(">Poblacion con afección: " + contar_afeccion_base() +
				"\n>Poblacion con enfermedad base: " + contar_enfermedad_base());
		System.out.println(">El promedio de edad de la Comunidad es: " + promedio_edad());
		numero_vacunas();
	}
	
	public int contar_afeccion_base() {
		int contador = 0;
		for (Ciudadano c: comunidad.getCiudadano()) {
			if (c.getObesidad()) {
				contador ++;
			}
		}
		return contador;
	}
	
	public int contar_enfermedad_base() {
		int contador = 0;
		for (Ciudadano c: comunidad.getCiudadano()) {
			if (c.getAsma()) {
				contador ++;
		
			}	
		}
		return contador;
	}
	
	public double promedio_edad() {
		double promedio = 0;
		for (Ciudadano c: comunidad.getCiudadano()) {
			promedio = promedio + c.getEdad();
		}
		return promedio/comunidad.getNro_ciudadano();
	}
	
	public void numero_vacunas() {
		int poblacion_vacunable = Math.round(comunidad.getNro_ciudadano() / 2);
		int disponible_vacuna1 = Math.round((poblacion_vacunable * 9) / 18);
		int disponible_vacuna2 = Math.round((poblacion_vacunable * 6) / 18);
		int disponible_vacuna3 = Math.round((poblacion_vacunable * 3) / 18);
		System.out.println("¡Vacunas disponibles!" +
		"\n>Vacuna 1 AstraZeneca: " + disponible_vacuna1 +
		"\n>Vacuna 2 CanSino: " + disponible_vacuna2 +
		"\n>Vacuna 3 Sinovac: " + disponible_vacuna3);
		
	}
	
}
	